require("dotenv/config");
const HDWalletProvider = require("@truffle/hdwallet-provider");
module.exports = {
  compilers: {
    solc: {
      version: "^0.8.0",
    },
  },
  networks: {
    development: {
      host: "localhost",
      port: 7545,
      network_id: "*",
    },
    rinkeby: {
      provider: () =>
        new HDWalletProvider(
          process.env.MNEMONIC,
          `https://rinkeby.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161`
        ),
      network_id: 4, // Rinkeby's id
      gas: 5500000, // Rinkeby has a lower block limit than mainnet
      confirmations: 2, // # of confs to wait between deployments. (default: 0)
      timeoutBlocks: 500, // # of blocks before a deployment times out  (minimum/default: 50)
      skipDryRun: true, // Skip dry run before migrations? (default: false for public nets )
    },
    mumbai: {
      provider: () =>
        new HDWalletProvider(
          process.env.MNEMONIC,
          `https://rpc-mumbai.maticvigil.com/`
        ),
      network_id: 80001, // Rinkeby's id
    },
  },
};
