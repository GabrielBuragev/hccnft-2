const HCCNFT = artifacts.require("HCCNFT");
module.exports = function (deployer) {
  require("dotenv/config");
  deployer.deploy(
    HCCNFT,
    "Highrollers Casino Club",
    "HCC",
    process.env.METADATA_BASE_URL,
    process.env.NOT_REVEALED_URL
  );
};
